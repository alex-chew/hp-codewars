from itertools import permutations
import re, sys

def main():
	with open(sys.argv[1], 'r+') as infile:
		rules = infile.readlines()
	people = [r.split(' ')[0] for r in rules]
	people.extend([r.split(' ')[-1].rstrip() for r in rules if re.match('\d', r.split(' ')[-1]) is None])
	people = list(set(people))
	solutions = []
	for p in list(permutations(people)):
		if valid(p, rules):
			solutions.append(p)
	print_solns(solutions)

def valid(perm, rules):
	for r in rules:
		r_nof = re.match(r'(\w+) NOT ON FLOOR (\d)', r)
		if r_nof is not None:
			if (perm.index(r_nof.group(1)) + 1) == int(r_nof.group(2)): return False
		
		r_nofo = re.match(r'(\w+) NOT ON FLOORS (\d) OR (\d)', r)
		if r_nofo is not None:
			if (perm.index(r_nofo.group(1)) + 1) in [int(r_nofo.group(2)), int(r_nofo.group(3))]: return False

		r_ohft = re.match(r'(\w+) ON HIGHER FLOOR THAN (\w+)', r)
		if r_ohft is not None:
			if not (perm.index(r_ohft.group(1))) > (perm.index(r_ohft.group(2))): return False

		r_oaft = re.match(r'^(\w+) ON ADJACENT FLOOR TO (\w+)', r)
		if r_oaft is not None:
			if abs(perm.index(r_oaft.group(1)) - perm.index(r_oaft.group(2))) > 1: return False

		r_noaft = re.match(r'(\w+) NOT ON ADJACENT FLOOR TO (\w+)', r)
		if r_noaft is not None:
			if not (abs(perm.index(r_noaft.group(1)) - perm.index(r_noaft.group(2))) > 1): return False
	return True

def print_solns(solns):
	print('{} solutions total:\n'.format(len(solns)))
	for s in solns:
		for p in reversed(list(enumerate(s))):
			print('{} {}'.format((p[0] + 1), p[1]))

if __name__ == '__main__':
	main()

